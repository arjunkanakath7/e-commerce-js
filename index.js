const firstproductTitle = document.getElementById('productTitle')
const mainImage = document.getElementById('mainImage')
const price = document.getElementById('price')
 const productDescription = document.getElementById('productDescription')

fetch('https://fakestoreapi.com/products')
.then(response => response.json())
.then(products => {
    const firstproduct = products[16]
    firstproductTitle.innerHTML = firstproduct.title
    mainImage.src = firstproduct.image
    price.innerHTML = firstproduct.price
    productDescription.innerHTML = firstproduct.description
})
.catch(error => {
    console.log(error)
})